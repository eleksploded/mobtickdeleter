package com.eleksploded.modtickdeleter;

import com.eleksploded.eleklib.config.Config;
import com.eleksploded.eleklib.config.ConfigBuilder;
import com.eleksploded.modtickdeleter.cap.ILastTicked;
import com.eleksploded.modtickdeleter.cap.LastTickedHandler;
import com.eleksploded.modtickdeleter.cap.LastTickedStorage;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

@Mod("mobtickdeleter")
public class MobTickDeleter {

    private static final Logger LOGGER = LogManager.getLogger();

    @CapabilityInject(ILastTicked.class)
    public static Capability<ILastTicked> lastTickedCapability;

    public static Config config;

    public MobTickDeleter() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);

        config = ConfigBuilder.builder()
                .addBool("debug", false, "Enable debug output")
                .addValue("blacklist", new ArrayList<String>(), "Mobs to ignore ticks")
                .addInt("tickTime", 20, "Amount of ticks if skipped to delete the mob", 1, 1024)
                .build("mobtickdeleter", ModConfig.Type.COMMON);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        CapabilityManager.INSTANCE.register(ILastTicked.class, new LastTickedStorage(), LastTickedHandler::new);
    }

    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {

    }

    public static void debug(String message) {
        if(config.getBool("debug")) {
            LOGGER.debug(message);
        }
    }
}
