package com.eleksploded.modtickdeleter;

import com.eleksploded.modtickdeleter.cap.ILastTicked;
import com.eleksploded.modtickdeleter.cap.LastTickedProvider;
import net.minecraft.entity.LivingEntity;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

import java.util.List;

@Mod.EventBusSubscriber
public class MobTickEventHandler {

    static List<String> blacklist = null;
    static Integer timeout = null;

    @SubscribeEvent
    public static void onModTick(LivingEvent.LivingUpdateEvent event) {
        if(blacklist == null || timeout == null) {
            MobTickDeleter.debug("Config Values not loaded");
            return;
        }

        if(!blacklist.contains(event.getEntityLiving().getType().getRegistryName())) {
            LazyOptional<ILastTicked> opt = event.getEntityLiving().getCapability(MobTickDeleter.lastTickedCapability);

            if(!opt.isPresent()) {
                MobTickDeleter.debug("No cap found for " + event.getEntityLiving().getName());
            }

            opt.ifPresent(cap -> {
                int lastTick = new Long(cap.getLastTicked()).intValue();
                int gameTick = new Long(event.getEntityLiving().world.getGameTime()).intValue();

                if(lastTick + timeout < gameTick) {
                    event.getEntityLiving().remove();
                } else {
                    cap.setLastTicked(gameTick);
                }
            });

        }
    }

    @SubscribeEvent
    public static void worldLoad(WorldEvent.Load e) {
        MobTickDeleter.debug("Loading Config Blacklist");
        blacklist = (List<String>) MobTickDeleter.config.getValue("blacklist");
        timeout = MobTickDeleter.config.getInt("tickTime");
    }

    @SubscribeEvent
    public static void attachCaps(AttachCapabilitiesEvent e) {
        if(e.getObject() instanceof LivingEntity) {
            e.addCapability(LastTickedProvider.key, new LastTickedProvider());
        }
    }
}
