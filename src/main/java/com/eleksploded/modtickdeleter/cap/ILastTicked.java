package com.eleksploded.modtickdeleter.cap;

public interface ILastTicked {
    long getLastTicked();
    void setLastTicked(long tick);
}
