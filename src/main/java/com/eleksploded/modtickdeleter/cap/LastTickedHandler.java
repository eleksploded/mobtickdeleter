package com.eleksploded.modtickdeleter.cap;

public class LastTickedHandler implements ILastTicked {

    long lastTick = 0;

    @Override
    public long getLastTicked() {
        return lastTick;
    }

    @Override
    public void setLastTicked(long tick) {
        lastTick = tick;
    }
}
