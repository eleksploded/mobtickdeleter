package com.eleksploded.modtickdeleter.cap;

import com.eleksploded.modtickdeleter.MobTickDeleter;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.util.LazyOptional;

public class LastTickedProvider implements ICapabilitySerializable<LongNBT> {

    public static ResourceLocation key = new ResourceLocation("mobtickdeleter", "lastticked");

    LastTickedHandler instance = new LastTickedHandler();
    LazyOptional<LastTickedHandler> lazy = LazyOptional.of(() -> instance);

    @Override
    public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
        if(cap == MobTickDeleter.lastTickedCapability) {
            return lazy.cast();
        } else {
            return LazyOptional.empty();
        }
    }

    @Override
    public LongNBT serializeNBT() {
        return (LongNBT) MobTickDeleter.lastTickedCapability.writeNBT(instance, null);
    }

    @Override
    public void deserializeNBT(LongNBT nbt) {
        MobTickDeleter.lastTickedCapability.readNBT(instance, null, nbt);
    }

}
