package com.eleksploded.modtickdeleter.cap;

import net.minecraft.nbt.INBT;
import net.minecraft.nbt.LongNBT;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;

public class LastTickedStorage implements Capability.IStorage<ILastTicked> {

    @Override
    public INBT writeNBT(Capability<ILastTicked> capability, ILastTicked instance, Direction side) {
        return LongNBT.valueOf(instance.getLastTicked());
    }

    @Override
    public void readNBT(Capability<ILastTicked> capability, ILastTicked instance, Direction side, INBT nbt) {
        instance.setLastTicked(((LongNBT) nbt).getLong());
    }

}
